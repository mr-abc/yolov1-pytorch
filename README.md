# [YoloV1 From Scratch - Pytorch](https://www.kaggle.com/code/vexxingbanana/yolov1-from-scratch-pytorch)

## Prepare data
see the [README.md](data/README.md) in `data` directory

## Train the model
```
python train.py                                      # train from last checkpoint, batch size: 16, epochs: 10
python train.py --batch-size 64 --epochs 100 --renew # train from begining
```

## Test the model
```
python test.py                                      # test from last checkpoint, batch size: 16
```

## Inference
```
python inference.py data/VOCdevkit/VOC2007/JPEGImages/000002.jpg --device cpu
```

## Resources
  - https://arxiv.org/abs/1506.02640
  - https://pjreddie.com/darknet/yolo/
  - https://www.bilibili.com/video/BV1xr4y1y7DD/?spm_id_from=333.337.search-card.all.click&vd_source=7d2f3e623fe725951776a6167dc1ece9
  - https://www.bilibili.com/video/BV1LK411973P?p=1
