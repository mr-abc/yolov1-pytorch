"""
Creates a Pytorch dataset to load the Pascal VOC dataset
"""
import torch

from torch.utils.data import Dataset
from PIL import Image


class VOCDataset(Dataset):
    def __init__(
        self,
        txt_file,
        S=7,
        B=2,
        C=20,
        transform=None,
    ):
        with open(txt_file, "r") as f:
            lines = f.readlines()

        self.image_list = [i.rstrip("\n") for i in lines]
        self.label_list = [
            str.replace("VOCdevkit", "VOClabels")
            .replace("JPEGImages", "labels")
            .replace(".jpg", ".txt")
            for str in self.image_list
        ]

        self.transform = transform
        self.S = S
        self.B = B
        self.C = C

    def __len__(self):
        return len(self.image_list)

    def __getitem__(self, index):
        boxes = []
        with open(self.label_list[index]) as f:
            for label in f.readlines():
                class_label, x, y, width, height = [
                    float(x) if float(x) != int(float(x)) else int(x)
                    for x in label.replace("\n", "").split()
                ]

                boxes.append([class_label, x, y, width, height])

        image = Image.open(self.image_list[index])
        boxes = torch.tensor(boxes)

        if self.transform:
            image, boxes = self.transform(image, boxes)

        # Convert To Cells
        label_matrix = torch.zeros((self.S, self.S, self.C + 5 * self.B))
        for box in boxes:
            class_label, x, y, width, height = box.tolist()
            class_label = int(class_label)

            # i,j represents the cell row and cell column
            i, j = int(self.S * y), int(self.S * x)
            y_cell, x_cell = self.S * y - i, self.S * x - j

            """
            Calculating the width and height of cell of bounding box,
            relative to the cell is done by the following, with
            width as the example:
            
            width_pixels = (width*self.image_width)
            cell_pixels = (self.image_width)
            
            Then to find the width relative to the cell is simply:
            width_pixels/cell_pixels, simplification leads to the
            formulas below.
            """
            width_cell, height_cell = (
                width * self.S,
                height * self.S,
            )

            # If no object already found for specific cell i,j
            # Note: This means we restrict to ONE object
            # per cell!
            if label_matrix[i, j, 20] == 0:
                # Set that there exists an object
                label_matrix[i, j, 20] = 1

                # Box coordinates
                box_coordinates = torch.tensor(
                    [x_cell, y_cell, width_cell, height_cell]
                )

                label_matrix[i, j, 21:25] = box_coordinates

                # Set one hot encoding for class_label
                label_matrix[i, j, class_label] = 1

        return image, label_matrix
