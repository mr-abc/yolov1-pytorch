## Requirements

```
conda create -n yolov1 python=3.10 -y
conda activate yolov1

pip install numpy
pip install matplotlib
pip install Pillow
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
```