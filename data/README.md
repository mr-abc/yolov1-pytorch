## Make VOC dataset
  - get_data.sh: download tar files
  - make_text.sh: generate txt files
    - train.txt
    - test.txt
  - voc_label.py: make label files outside of `VOCdevkit`

To make VOC dataset, in the `data` directory, execute the following shell commands
```
bash get_data.sh
bash make_text.sh
```
  
If you get data to the directory outside of the project, make sure to create a link to `VOCdevkit` in the `data` directory